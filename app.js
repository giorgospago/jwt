const express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const jwt_secret = "w345t53";

function authCheck(req, res, next){
    const my_token = req.headers.authorization.replace('Bearer ','');
    jwt.verify(my_token, jwt_secret, (err, decoded) => {
        if(err)
            return res.sendStatus(403);
        
        req['token'] = my_token;
        req['decoded'] = decoded;
        next();
    });
}

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/api", (req, res) => {
    res.json({
        message: "Api is working..."
    });
});

app.post("/api/save-user", authCheck, (req, res) => {
    res.json({
        message: "User Saved...",
        data: req.decoded,
        token: req.token
    });
});

app.get("/api/login", (req, res) => {
    
    // authentication....

    const user = {
        id: 5,
        email: "test@test.gr",
        username: "karydakis"
    };

    const token = jwt.sign(user,jwt_secret,{ expiresIn: '5h' });
    
    res.json({
        token
    });
});

app.listen(5000, () => {
    console.log("app runs at 5000 port");
});